/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef ST_SCRIPT_H
#define ST_SCRIPT_H

#define DB_CREATE_GLOBAL "\
CREATE TABLE IF NOT EXISTS STGlobal  \
( \
    num INTEGER Primary Key, \
    data INTEGER \
);"

#define DB_CREATE_BELLS "\
CREATE TABLE IF NOT EXISTS STBells  \
( \
    num INTEGER Primary Key, \
    h INTEGER, \
    m INTEGER \
);"

#define DB_CREATE_PAIRTYPE "\
CREATE TABLE IF NOT EXISTS STPairType \
( \
    id INTEGER Primary Key, \
    fullName TEXT, \
    shortName TEXT, \
    check( shortName is NOT NULL and shortName<>\'\' ) \
);"

#define DB_CREATE_TEACHER "\
CREATE TABLE IF NOT EXISTS STTeacher \
( \
    id INTEGER Primary Key, \
    name TEXT, \
    nick TEXT, \
    additions TEXT, \
    check( nick is NOT NULL and nick<>\'\' ) \
);"

#define DB_CREATE_SUBJECT "\
CREATE TABLE IF NOT EXISTS STSubject \
( \
    id INTEGER Primary Key, \
    name TEXT, \
    nick TEXT, \
    check( nick is NOT NULL and nick<>\'\' ) \
);"

#define DB_CREATE_TASK "\
CREATE TABLE IF NOT EXISTS STTask  \
( \
    id INTEGER Primary Key, \
    subjectId INTEGER, \
    seriesNumber INTEGER, \
    isComplete BOOLEAN, \
    dateD INTEGER, \
    dateM INTEGER, \
    dateY INTEGER, \
    task TEXT, \
    description TEXT, \
    prior INTEGER, \
    check( task is NOT NULL and task<>\'\' ), \
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade \
);"

#define DB_CREATE_PAIR "\
CREATE TABLE IF NOT EXISTS STPair \
( \
    id INTEGER Primary Key, \
    subjectId INTEGER NOT NULL, \
    typeId INTEGER, \
    teacherId INTEGER, \
    week INTEGER, \
    dayOfWeek INTEGER, \
    gridNumber INTEGER, \
    auditory TEXT, \
    additions TEXT, \
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade, \
    foreign Key (typeId) references STPairType (id) on delete cascade on update cascade, \
    foreign Key (teacherId) references STTeacher (id) on delete cascade on update cascade, \
    foreign Key (gridNumber) references STBells (num) on delete cascade on update cascade \
);"

#endif // ST_SCRIPT_H
