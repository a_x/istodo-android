#include "st.task.h"

STTask::STTask(QObject *inParent) :
    QObject(inParent)
{
    this->clear();
}

STTask::STTask(int inId,
               int inSubjectId,
               int inSeriesNumber,
               bool inIsComplete,
               QDate inDate,
               QString inTask,
               QString inDescription,
               int inPrior,
               QObject *inParent) :
       QObject(inParent)
{
    _id = inId;
    _subjectId = inSubjectId;
    _seriesNumber = inSeriesNumber;
    _isComplete = inIsComplete;
    _date = inDate;
    _task = inTask;
    _description = inDescription;
    _prior = inPrior;
}

int STTask::id() const
{
    return _id;
}

int STTask::subjectId() const
{
    return _subjectId;
}

int STTask::seriesNumber() const
{
    return _seriesNumber;
}

bool STTask::isComplete() const
{
    return _isComplete;
}

QDate STTask::date() const
{
    return _date;
}

QString STTask::task() const
{
    return _task;
}

QString STTask::description() const
{
    return _description;
}

int STTask::prior() const
{
    return _prior;
}

bool STTask::isOutdated()
{
    if (!isComplete()) {
        return QDate::currentDate() > date();
    } else {
        return false;
    }
}

void STTask::setId(int arg)
{
    if (_id != arg) {
        _id = arg;
        emit idChanged(arg);
    }
}

void STTask::setSubjectId(int arg)
{
    if (_subjectId != arg) {
        _subjectId = arg;
        emit subjectIdChanged(arg);
    }
}

void STTask::setSeriesNumber(int arg)
{
    if (_seriesNumber != arg) {
        _seriesNumber = arg;
        emit seriesNumberChanged(arg);
    }
}

void STTask::setIsComplete(bool arg)
{
    if (_isComplete != arg) {
        _isComplete = arg;
        emit isCompleteChanged(arg);
    }
}

void STTask::setDate(QDate arg)
{
    if (_date != arg) {
        _date = arg;
        emit dateChanged(arg);
    }
}

void STTask::setTask(QString arg)
{
    if (_task != arg) {
        _task = arg;
        emit taskChanged(arg);
    }
}

void STTask::setDescription(QString arg)
{
    if (_description != arg) {
        _description = arg;
        emit descriptionChanged(arg);
    }
}

void STTask::setPrior(int arg)
{
    if (_prior != arg) {
        _prior = arg;
        emit priorChanged(arg);
    }
}

void STTask::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_TASK);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STTask::dbSave()
{
    QSqlQuery query;
    if (_id == DB_INVALID_ID) {
        query.prepare("insert into STTask values (NULL,?,?,?,?,?,?,?,?,?);");
        query.addBindValue( STDataProvider::getVariantFromId(_subjectId) );
        query.addBindValue(_seriesNumber);
        query.addBindValue(_isComplete);
        query.addBindValue(_date.day());
        query.addBindValue(_date.month());
        query.addBindValue(_date.year());
        query.addBindValue(_task);
        query.addBindValue(_description);
        query.addBindValue(_prior);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        setId( query.lastInsertId().toInt() );

        STDataProvider::dp()->objectSaved(this, true);
    } else {
        query.prepare("update STTask set subjectId=?, seriesNumber=?, isComplete=?, dateD=?, dateM=?, dateY=?, task=?, description=?, prior=? where id=?;");
        query.addBindValue( STDataProvider::getVariantFromId(_subjectId) );
        query.addBindValue(_seriesNumber);
        query.addBindValue(_isComplete);
        query.addBindValue(_date.day());
        query.addBindValue(_date.month());
        query.addBindValue(_date.year());
        query.addBindValue(_task);
        query.addBindValue(_description);
        query.addBindValue(_prior);
        query.addBindValue(_id);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        STDataProvider::dp()->objectSaved(this, false);
    }
}

void STTask::dbRemove()
{
    if (_id == DB_INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    QSqlQuery query;
    query.prepare("delete from STTask where id=?;");
    query.addBindValue(_id);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    STDataProvider::dp()->objectRemoved(this);
}

void STTask::clear()
{
    _id = DB_INVALID_ID;
    _subjectId = DB_INVALID_ID;
    _seriesNumber = DB_INVALID_ID;
    _isComplete = 0;
    _date = QDate();
    _task.clear();
    _description.clear();
    _prior = 0;
}
