#include "st.dataprovider.h"

#include "st.globaltable.h"
#include "st.pair.h"
#include "st.pairtype.h"
#include "st.subject.h"
#include "st.task.h"
#include "st.teacher.h"

static STDataProvider* _qmlDp = NULL;

STDataProvider::STDataProvider(QString inFileName, QObject *parent) :
    QObject(parent)
{
    _firstLaunch = false;
    loadFromFile(inFileName);
    _currentDate = QDate::currentDate();
}

QString STDataProvider::getDatabaseName()
{
    return QSqlDatabase::database().databaseName();
}

void STDataProvider::loadFromFile(QString inFileName)
{
    QString dbName;
    // Get name
    if (inFileName.isEmpty() ||
            inFileName.isNull() ||
            !QFile(inFileName).exists()) {
        QString dataPath = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        if (!QDir(dataPath).exists()){
            QDir().mkpath(dataPath);
        }

        dbName = dataPath + QDir::separator() + DB_NAME;
    } else {
        dbName = inFileName;
    }

    qDebug() << "Load database from file: " << dbName;
    if( !QFile(dbName).exists() ) {
        _firstLaunch = true;
    }

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbName);
    if (!db.open()) {
        qFatal( "%s", db.lastError().text().toStdString().c_str() );
    }

    STGlobalTable::dbCreateTable();
    STBell::dbCreateTable();
    STPairType::dbCreateTable();
    STTeacher::dbCreateTable();
    STSubject::dbCreateTable();
    STTask::dbCreateTable();
    STPair::dbCreateTable();

    refillAll();
}

void STDataProvider::importFromFile(QString fileName)
{
    if (fileName.isEmpty() ||
            fileName.isNull() ||
            !QFile(fileName).exists()) {
        return;
    }

    QString dbName = getDatabaseName();

    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query;
    query.clear();
    db.close();

    if (!QFile::remove(dbName)) {
        qDebug() << "Error: can't remove database file";
        return;
    }


    if (!QFile::copy(fileName, dbName)) {
        qDebug() << "Error: can't copy database file";
        return;
    }

    loadFromFile(dbName);
}

QVariant STDataProvider::getVariantFromId(int inId)
{
    if (inId == DB_INVALID_ID) {
        return QVariant(QVariant::Int);
    }
    return QVariant(inId);
}

int STDataProvider::getIdFromVariant(QVariant inVar)
{
    if (inVar.isNull() ||
            (inVar.type() != QVariant::Int &&
             inVar.type() != QVariant::LongLong)) {
        return DB_INVALID_ID;
    }
    return inVar.toInt();
}

STDataProvider *STDataProvider::dp()
{
    return _qmlDp;
}

bool STDataProvider::isEmpty()
{
    return _firstLaunch;
}

STDataProvider* STDataProvider::qmlDp()
{
    return _qmlDp;
}

STGlobalTable STDataProvider::globalTable() const
{
    return *_globalTable;
}

QQmlListProperty<STBell> STDataProvider::bells()
{
    return QQmlListProperty<STBell>(this, _bells);
}

QQmlListProperty<STPair> STDataProvider::pairs()
{
    return QQmlListProperty<STPair>(this, _pairs);
}

QQmlListProperty<STPairType> STDataProvider::pairTypes()
{
    return QQmlListProperty<STPairType>(this, _pairTypes);
}

QQmlListProperty<STSubject> STDataProvider::subjects()
{
    return QQmlListProperty<STSubject>(this, _subjects);
}

QQmlListProperty<STTask> STDataProvider::tasks()
{
    return QQmlListProperty<STTask>(this, _tasks);
}

QQmlListProperty<STTeacher> STDataProvider::teachers()
{
    return QQmlListProperty<STTeacher>(this, _teachers);
}

QString STDataProvider::shortDayName(int day) const
{
    return QDate::shortDayName(day);
}

QString STDataProvider::shortMonthName(int month) const
{
    return QDate::shortMonthName(month);
}

QDate STDataProvider::currentDate() const
{
    return _currentDate;
}

void STDataProvider::addDays(int count)
{
    _currentDate = _currentDate.addDays(count);
    emit currentDateChanged(_currentDate);

    // Update models with current date relation
    emit pairsChanged( pairs() );
    emit tasksChanged( tasks() );
}

int STDataProvider::getCountOfWeeks() const
{
    QSqlQuery query;
    query.prepare("select max(week) from STPair;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
    query.next();

    return query.value(0).toInt();
}

int STDataProvider::getWeekNumber() const
{
    int cycleWeekCount = getCountOfWeeks();
    int cycleWeekNumber = getWeekSemestrNumber() % cycleWeekCount;
    if (cycleWeekNumber == 0) {
        cycleWeekNumber = cycleWeekCount;
    }
    return cycleWeekNumber;
}

int STDataProvider::getWeekSemestrNumber() const
{
    QDate begin = _globalTable->beginDate();

    if (_currentDate < begin) {
        return -1;
    }

    int week = 1;
    if( begin.dayOfWeek() != Qt::Monday) {
        int dayOfWeek = begin.dayOfWeek();
        while( dayOfWeek != Qt::Monday ) {
            if( begin == _currentDate ) {
                return week;
            }
            begin = begin.addDays(1);
            dayOfWeek = begin.dayOfWeek();
        }

        week++;
    }

    return week+begin.daysTo(_currentDate)/7;
}

STBell* STDataProvider::getBell(int num)
{
    for (int i = 0; i < _bells.size(); i++) {
        STBell *iBell = _bells.at(i);
        if (iBell->num() == num) {
            return iBell;
        }
    }
    return NULL;
}

template<class T> T* findById(QList<T*> list, int id)
{
    for (int i = 0; i < list.size(); i++) {
        T *iElm = list.at(i);
        if (iElm->id() == id) {
            return iElm;
        }
    }
    return NULL;
}

STPair* STDataProvider::getPair(int id)
{
    return findById<STPair>(_pairs, id);
}

STPairType* STDataProvider::getPairType(int id)
{
    return findById<STPairType>(_pairTypes, id);
}

STSubject* STDataProvider::getSubject(int id)
{
    return findById<STSubject>(_subjects, id);
}

STTask* STDataProvider::getTask(int id)
{
    if(id == DB_INVALID_ID){
        return new STTask();
    }

    return findById<STTask>(_tasks, id);
}

STTeacher* STDataProvider::getTeacher(int id)
{
    return findById<STTeacher>(_teachers, id);
}

void STDataProvider::setQmlDp(STDataProvider *arg)
{
    if (_qmlDp == NULL) {
        _qmlDp = arg;
        emit qmlDpChanged(arg);
    }
}

void STDataProvider::setCurrentDate(QDate arg)
{
    if (_currentDate != arg) {
        _currentDate = arg;
        emit currentDateChanged(arg);

        // Update models with current date relation
        emit pairsChanged( pairs() );
        emit tasksChanged( tasks() );
    }

}

void STDataProvider::objectSaved(QObject *obj, bool newObj)
{
    if (dynamic_cast<STBell*>(obj)) {
        if (newObj) {
            _bells.append((STBell*)obj);
        }
        emit bellsChanged( bells() );
    } else if (dynamic_cast<STPair*>(obj)) {
        if (newObj) {
            _pairs.append((STPair*)obj);
        }
        emit pairsChanged( pairs() );
    } else if (dynamic_cast<STPairType*>(obj)) {
        if (newObj) {
            _pairTypes.append((STPairType*)obj);
        }
        emit pairTypesChanged( pairTypes() );
    } else if (dynamic_cast<STSubject*>(obj)) {
        if (newObj) {
            _subjects.append((STSubject*)obj);
        }
        emit subjectsChanged( subjects() );
    } else if (dynamic_cast<STTask*>(obj)) {
        if (newObj) {
            _tasks.append((STTask*)obj);
        }
        emit tasksChanged( tasks() );
    } else if (dynamic_cast<STTeacher*>(obj)) {
        if (newObj) {
            _teachers.append((STTeacher*)obj);
        }
        emit teachersChanged( teachers() );
    } else {
        qDebug() << "Unknow object type: " << obj;
    }
}

void STDataProvider::objectRemoved(QObject *obj)
{
    if (dynamic_cast<STBell*>(obj)) {
        refillBells();
        refillPairs();
    } else if (dynamic_cast<STPair*>(obj)) {
        refillPairs();
    } else if (dynamic_cast<STPairType*>(obj)) {
        refillPairTypes();
        refillPairs();
    } else if (dynamic_cast<STSubject*>(obj)) {
        refillSubjects();
        refillTasks();
        refillPairs();
    } else if (dynamic_cast<STTask*>(obj)) {
        refillTasks();
    } else if (dynamic_cast<STTeacher*>(obj)) {
        refillTeachers();
        refillPairs();
    } else {
        qDebug() << "Unknow object type: " << obj;
    }
}

void STDataProvider::refillAll()
{
    refillGlobalTable();
    refillBells();
    refillPairTypes();
    refillTeachers();
    refillSubjects();
    refillTasks();
    refillPairs();
}

void STDataProvider::refillGlobalTable()
{
    int version;
    int pairLength;
    QDate beginDate;
    QDate endDate;

    QSqlQuery query;
    query.prepare("select data from STGlobal where num=?;");
    query.addBindValue(STGlobalTable::db_version);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
    query.next();
    version = query.value(0).toInt();

    query.clear();
    query.prepare("select data from STGlobal where num=?;");
    query.addBindValue(STGlobalTable::db_pairLength);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
    query.next();
    pairLength = query.value(0).toInt();

    // Read begin date
    query.clear();
    query.prepare("select num,data from STGlobal where num>=? and num<=?;");
    query.addBindValue(STGlobalTable::db_dBegin);
    query.addBindValue(STGlobalTable::db_yBegin);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    int dBegin = -1;
    int mBegin = -1;
    int yBegin = -1;
    while (query.next()) {
        switch (query.value(0).toInt()) {
            case STGlobalTable::db_dBegin: {
                dBegin = query.value(1).toInt();
                break;
            }
            case STGlobalTable::db_mBegin: {
                mBegin = query.value(1).toInt();
                break;
            }
            case STGlobalTable::db_yBegin: {
                yBegin = query.value(1).toInt();
                break;
            }
            default: {
                qFatal( "Unknow num of key" );
            }
        }
    }
    beginDate = QDate(yBegin,mBegin,dBegin);

    // Read end date
    query.clear();
    query.prepare("select num,data from STGlobal where num>=? and num<=?;");
    query.addBindValue(STGlobalTable::db_dEnd);
    query.addBindValue(STGlobalTable::db_yEnd);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    int dEnd = -1;
    int mEnd = -1;
    int yEnd = -1;
    while (query.next()) {
        switch (query.value(0).toInt()) {
            case STGlobalTable::db_dEnd: {
                dEnd = query.value(1).toInt();
                break;
            }
            case STGlobalTable::db_mEnd: {
                mEnd = query.value(1).toInt();
                break;
            }
            case STGlobalTable::db_yEnd: {
                yEnd = query.value(1).toInt();
                break;
            }
            default: {
                qFatal( "Unknow num of key" );
            }
        }
    }
    endDate = QDate(yEnd,mEnd,dEnd);

    _globalTable = new STGlobalTable(version,
                                 pairLength,
                                 beginDate,
                                 endDate,
                                 this);

    emit globalTableChanged(*_globalTable);
}

void STDataProvider::refillBells()
{
    QSqlQuery query;
    query.prepare("select * from STBells;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    //qDeleteAll(_bells);
    _bells.clear();
    while (query.next()) {
        STBell *bell = new STBell(query.value(0).toInt(),
                                  QTime(query.value(1).toInt(), query.value(2).toInt()),
                                  this);
        _bells.push_back(bell);
    }

    emit bellsChanged( bells() );
}

void STDataProvider::refillPairs()
{
    QSqlQuery query;
    query.prepare("select * from STPair;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    //qDeleteAll(_pairs);
    _pairs.clear();
    while (query.next()) {
        STPair *pair = new STPair(query.value(0).toInt(),
                                  getIdFromVariant( query.value(1) ),
                                  getIdFromVariant( query.value(2) ),
                                  getIdFromVariant( query.value(3) ),
                                  query.value(4).toInt(),
                                  query.value(5).toInt(),
                                  query.value(6).toInt(),
                                  query.value(7).toString(),
                                  query.value(8).toString(),
                                  this);
        _pairs.push_back(pair);
    }

    emit pairsChanged( pairs() );
}

void STDataProvider::refillPairTypes()
{
    QSqlQuery query;
    query.prepare("select * from STPairType;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    //qDeleteAll(_pairTypes);
    _pairTypes.clear();
    while (query.next()) {
        STPairType *pairType = new STPairType(query.value(0).toInt(),
                                              query.value(1).toString(),
                                              query.value(2).toString(),
                                              this);
        _pairTypes.push_back(pairType);
    }

    emit pairTypesChanged( pairTypes() );
}

void STDataProvider::refillSubjects()
{
    QSqlQuery query;
    query.prepare("select * from STSubject;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    //qDeleteAll(_subjects);
    _subjects.clear();
    while (query.next()) {
        STSubject *subject = new STSubject(query.value(0).toInt(),
                                           query.value(1).toString(),
                                           query.value(2).toString(),
                                           this);
        _subjects.push_back(subject);
    }

    emit subjectsChanged( subjects() );
}

void STDataProvider::refillTasks()
{
    QSqlQuery query;
    query.prepare("select * from STTask;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    //qDeleteAll(_tasks);
    _tasks.clear();
    while (query.next()) {
        STTask *task = new STTask(query.value(0).toInt(),
                                  getIdFromVariant( query.value(1) ),
                                  query.value(2).toInt(),
                                  query.value(3).toBool(),
                                  QDate( query.value(6).toInt(),
                                         query.value(5).toInt(),
                                         query.value(4).toInt() ),
                                  query.value(7).toString(),
                                  query.value(8).toString(),
                                  query.value(9).toInt(),
                                  this);
        _tasks.push_back(task);
    }

    emit tasksChanged( tasks() );
}

void STDataProvider::refillTeachers()
{
    QSqlQuery query;
    query.prepare("select * from STTeacher;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    //qDeleteAll(_teachers);
    _teachers.clear();
    while (query.next()) {
        STTeacher *teacher = new STTeacher(query.value(0).toInt(),
                                           query.value(1).toString(),
                                           query.value(2).toString(),
                                           query.value(3).toString(),
                                           this);
        _teachers.push_back(teacher);
    }

    emit teachersChanged( teachers() );
}
