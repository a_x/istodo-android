#include "st.dropbox.h"

STDropbox::STDropbox(QObject *parent) :
    QObject(parent)
{
    dropbox = new QDropbox("only_in_version", "in_google_play");
    qDebug() << "!!! Sorry, but it is private key !!!";

    connect(dropbox, SIGNAL(tokenExpired()),
            this, SLOT(openAuthorizeUrl()));
    connect(dropbox, SIGNAL(requestTokenFinished(QString,QString)),
            this, SLOT(continueProcess()));
    connect(dropbox, SIGNAL(accessTokenFinished(QString,QString)),
            this, SLOT(authorized()));
    connect(dropbox, SIGNAL(errorOccured(QDropbox::Error)),
            this, SLOT(dropboxError()));
}

STDropbox::~STDropbox()
{
    // Memory leak or crash...
    // delete dropbox;
}

void STDropbox::startImport()
{
    this->isImport = true;
    start();
}

void STDropbox::startExport()
{
    this->isImport = false;
    start();
}

void STDropbox::continueProcess()
{
    dropbox->requestAccessToken();
}

void STDropbox::start()
{
    QSettings settings;
    if (settings.value(SET_TOKEN).toString().isEmpty() ||
            settings.value(SET_TOKEN_SECRET).toString().isEmpty()) {
        dropbox->requestToken();
    } else {
        dropbox->setToken( settings.value(SET_TOKEN).toString() );
        dropbox->setTokenSecret( settings.value(SET_TOKEN_SECRET).toString() );
        this->authorized();
    }
}

void STDropbox::openAuthorizeUrl()
{
    QDesktopServices::openUrl(dropbox->authorizeLink());

    emit needConfirmOnSite();
}

void STDropbox::authorized()
{
    QSettings settings;
    settings.setValue(SET_TOKEN, dropbox->token());
    settings.setValue(SET_TOKEN_SECRET, dropbox->tokenSecret());

    QDropboxFile dbFile("/sandbox/iStodo.db", dropbox);
    if (isImport) {
        if (!dbFile.open(QIODevice::ReadOnly)) {
            qDebug() << "db file open error";
            emit error();
            return;
        }

        QString path =
                QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        if (!QDir(path).exists()) {
            QDir().mkpath(path);
        }
        QString name = path + QDir::separator() + "dbTempFile";

        QFile tempFile(name);
        if (!tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            qDebug() << "db file open error";
            emit error();
            return;
        }

        tempFile.write( dbFile.readAll() );
        tempFile.close();

        STDataProvider::dp()->importFromFile(name);

        emit importFinished();
    }
    else {
        if (!dbFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            qDebug() << "db file open error";
            emit error();
            return;
        }

        QFile istodoFile( STDataProvider::dp()->getDatabaseName() );
        if (!istodoFile.open(QIODevice::ReadOnly)) {
            qDebug() << "istodo file open error";
            emit error();
            return;
        }

        dbFile.write( istodoFile.readAll() );

        emit exportFinished();
    }
}

void STDropbox::dropboxError()
{
    emit error();
}
