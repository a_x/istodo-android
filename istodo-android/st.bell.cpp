#include "st.bell.h"

STBell::STBell(QObject *parent) :
    QObject(parent)
{
    this->clear();
}

STBell::STBell(int inNum, QTime inTime, QObject *parent) :
    QObject(parent)
{
    _num = inNum;
    _time = inTime;
}

int STBell::num() const
{
    return _num;
}

QTime STBell::time() const
{
    return _time;
}

void STBell::setNum(int arg)
{
    if (_num != arg) {
        _num = arg;
        emit numChanged(arg);
    }
}

void STBell::setTime(QTime arg)
{
    if (_time != arg) {
        _time = arg;
        emit timeChanged(arg);
    }
}

void STBell::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_BELLS);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STBell::dbSave()
{
    QSqlQuery query;
    if (_num == DB_INVALID_ID) {
        query.prepare("insert into STBells values (NULL,?,?);");
        query.addBindValue(_time.hour());
        query.addBindValue(_time.minute());
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        setNum( query.lastInsertId().toInt() );

        STDataProvider::dp()->objectSaved(this, true);
    } else {
        query.prepare("update STBells set h=?, m=? where num=?;");
        query.addBindValue(_time.hour());
        query.addBindValue(_time.minute());
        query.addBindValue(_num);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        STDataProvider::dp()->objectSaved(this, false);
    }
}

void STBell::dbRemove()
{
    if (_num == DB_INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    QSqlQuery query;
    query.prepare("delete from STBells where num=?;");
    query.addBindValue(_num);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    STDataProvider::dp()->objectRemoved(this);
}

void STBell::clear()
{
    _num = DB_INVALID_ID;
    _time = QTime();
}
