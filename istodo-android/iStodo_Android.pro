TEMPLATE = app
TARGET = iStodo

QT += qml quick widgets network sql
QTPLUGIN += qsqlite

android {
    QT += androidextras xml svg
    QTPLUGIN += qsvg
}

SOURCES += main.cpp \
    st.bell.cpp \
    st.dataprovider.cpp \
    st.dropbox.cpp \
    st.globaltable.cpp \
    st.pair.cpp \
    st.pairtype.cpp \
    st.subject.cpp \
    st.task.cpp \
    st.teacher.cpp


RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
include(../qtdropbox/qtdropbox.pri)
HEADERS += \
    st.bell.h \
    st.dataprovider.h \
    st.dropbox.h \
    st.globaltable.h \
    st.include.h \
    st.pair.h \
    st.pairtype.h \
    st.script.h \
    st.subject.h \
    st.task.h \
    st.teacher.h

OTHER_FILES += \
    ActionBar/ACActionBar.qml \
    ActionBar/ACActionButton.qml \
    ActionBar/ACBackButton.qml \
    ActionBar/ACNavDrawerButton.qml \
    Controls/ACNavigationDrawer.qml \
    Controls/ACTextField.qml \
    Controls/ACToast.qml \
    Dialogs/ACDatePicker.qml \
    Dialogs/ACDialog.qml \
    Dialogs/ACDialogButton.qml \
    Dialogs/ACPicker.qml \
    Dialogs/ACPickerList.qml \
    Dialogs/ACTimePicker.qml \
    Global/ACGlobal.qml \
    Global/ACStyle.qml \
    Global/qmldir \
    Other/ACBorder.qml \
    Other/ACDarkBackground.qml \
    Other/ACStackView.qml \
    Other/ACTitleLabel.qml \
    images/icons/addIcon.svg \
    images/icons/backIcon.svg \
    images/icons/calendarIcon.svg \
    images/icons/cancelIcon.svg \
    images/icons/doneIcon.svg \
    images/icons/editIcon.svg \
    images/icons/findIcon.svg \
    images/icons/logoIcon.svg \
    images/icons/navDrawerIcon.svg \
    images/icons/overflowIcon.svg \
    images/icons/pickerShadowDown.svg \
    images/icons/pickerShadowUp.svg \
    images/icons/refreshIcon.svg \
    images/icons/trashIcon.svg \
    AppFiles/ACNavigationController.qml \
    AppFiles/main.qml \
    AppFiles/STSynkView.qml \
    AppFiles/STDay.qml \
    AppFiles/STWeek.qml \
    AppFiles/STDayTasks.qml \
    AppFiles/STDaySchedule.qml \
    AppFiles/STWeekTasks.qml \
    AppFiles/STWeekSchedule.qml \
    AppFiles/STStartView.qml \
    Controls/ACButton.qml \
    Other/ACSwipeArea.qml \
    AppFiles/STGesturesHelper.qml \
    ActionBar/ACCalendarButton.qml \
    android/AndroidManifest.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
