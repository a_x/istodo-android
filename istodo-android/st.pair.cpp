#include "st.pair.h"

STPair::STPair(QObject *inParent) :
    QObject(inParent)
{
    this->clear();
}

STPair::STPair(int inId,
               int inSubjectId,
               int inTypeId,
               int inTeacherId,
               int inWeek,
               int inDayOfWeek,
               int inGridNumber,
               QString inAuditory,
               QString inAdditions,
               QObject *inParent) :
    QObject(inParent)
{
    _id = inId;
    _subjectId = inSubjectId;
    _typeId = inTypeId;
    _teacherId = inTeacherId;
    _week = inWeek;
    _dayOfWeek = inDayOfWeek;
    _gridNumber = inGridNumber;
    _auditory = inAuditory;
    _additions = inAdditions;
}

int STPair::id() const
{
    return _id;
}

int STPair::subjectId() const
{
    return _subjectId;
}

int STPair::typeId() const
{
    return _typeId;
}

int STPair::teacherId() const
{
    return _teacherId;
}

int STPair::week() const
{
    return _week;
}

int STPair::dayOfWeek() const
{
    return _dayOfWeek;
}

int STPair::gridNumber() const
{
    return _gridNumber;
}

QString STPair::auditory() const
{
    return _auditory;
}

QString STPair::additions() const
{
    return _additions;
}

void STPair::setId(int arg)
{
    if (_id != arg) {
        _id = arg;
        emit idChanged(arg);
    }
}

void STPair::setSubjectId(int arg)
{
    if (_subjectId != arg) {
        _subjectId = arg;
        emit subjectIdChanged(arg);
    }
}

void STPair::setTypeId(int arg)
{
    if (_typeId != arg) {
        _typeId = arg;
        emit typeIdChanged(arg);
    }
}

void STPair::setTeacherId(int arg)
{
    if (_teacherId != arg) {
        _teacherId = arg;
        emit teacherIdChanged(arg);
    }
}

void STPair::setWeek(int arg)
{
    if (_week != arg) {
        _week = arg;
        emit weekChanged(arg);
    }
}

void STPair::setDayOfWeek(int arg)
{
    if (_dayOfWeek != arg) {
        _dayOfWeek = arg;
        emit dayOfWeekChanged(arg);
    }
}

void STPair::setGridNumber(int arg)
{
    if (_gridNumber != arg) {
        _gridNumber = arg;
        emit gridNumberChanged(arg);
    }
}

void STPair::setAuditory(QString arg)
{
    if (_auditory != arg) {
        _auditory = arg;
        emit auditoryChanged(arg);
    }
}

void STPair::setAdditions(QString arg)
{
    if (_additions != arg) {
        _additions = arg;
        emit additionsChanged(arg);
    }
}

void STPair::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_PAIR);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STPair::dbSave()
{
    QSqlQuery query;
    if (_id == DB_INVALID_ID) {
        query.prepare("insert into STPair values (NULL,?,?,?,?,?,?,?,?);");
        query.addBindValue( STDataProvider::getVariantFromId(_subjectId) );
        query.addBindValue( STDataProvider::getVariantFromId(_typeId) );
        query.addBindValue( STDataProvider::getVariantFromId(_teacherId) );
        query.addBindValue(_week);
        query.addBindValue(_dayOfWeek);
        query.addBindValue(_gridNumber);
        query.addBindValue(_auditory);
        query.addBindValue(_additions);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        setId( query.lastInsertId().toInt() );

        STDataProvider::dp()->objectSaved(this, true);
    } else {
        query.prepare("update STPair set subjectId=?, typeId=?, teacherId=?, week=?, dayOfWeek=?, gridNumber=?, auditory=?, additions=? where id=?;");
        query.addBindValue( STDataProvider::getVariantFromId(_subjectId) );
        query.addBindValue( STDataProvider::getVariantFromId(_typeId) );
        query.addBindValue( STDataProvider::getVariantFromId(_teacherId) );
        query.addBindValue(_week);
        query.addBindValue(_dayOfWeek);
        query.addBindValue(_gridNumber);
        query.addBindValue(_auditory);
        query.addBindValue(_additions);
        query.addBindValue(_id);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        STDataProvider::dp()->objectSaved(this, false);
    }
}

void STPair::dbRemove()
{
    if (_id == DB_INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    QSqlQuery query;
    query.prepare("delete from STPair where id=?;");
    query.addBindValue(_id);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    STDataProvider::dp()->objectRemoved(this);
}

void STPair::clear()
{
    _id = DB_INVALID_ID;
    _subjectId = DB_INVALID_ID;
    _typeId = DB_INVALID_ID;
    _teacherId = DB_INVALID_ID;
    _week = DB_INVALID_ID;
    _dayOfWeek = DB_INVALID_ID;
    _gridNumber = DB_INVALID_ID;
    _auditory.clear();
    _additions.clear();
}
