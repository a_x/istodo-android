/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef ST_PAIR_H
#define ST_PAIR_H

#include "st.include.h"

class STPair : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int subjectId READ subjectId WRITE setSubjectId NOTIFY subjectIdChanged)
    Q_PROPERTY(int typeId READ typeId WRITE setTypeId NOTIFY typeIdChanged)
    Q_PROPERTY(int teacherId READ teacherId WRITE setTeacherId NOTIFY teacherIdChanged)
    Q_PROPERTY(int week READ week WRITE setWeek NOTIFY weekChanged)
    Q_PROPERTY(int dayOfWeek READ dayOfWeek WRITE setDayOfWeek NOTIFY dayOfWeekChanged)
    Q_PROPERTY(int gridNumber READ gridNumber WRITE setGridNumber NOTIFY gridNumberChanged)
    Q_PROPERTY(QString auditory READ auditory WRITE setAuditory NOTIFY auditoryChanged)
    Q_PROPERTY(QString additions READ additions WRITE setAdditions NOTIFY additionsChanged)

signals:
    void idChanged(int arg);
    void subjectIdChanged(int arg);
    void typeIdChanged(int arg);
    void teacherIdChanged(int arg);
    void weekChanged(int arg);
    void dayOfWeekChanged(int arg);
    void gridNumberChanged(int arg);
    void auditoryChanged(QString arg);
    void additionsChanged(QString arg);

public:
    STPair(QObject *inParent = 0);
    STPair(int inId,
           int inSubjectId,
           int inTypeId,
           int inTeacherId,
           int inWeek,
           int inDayOfWeek,
           int inGridNumber,
           QString inAuditory,
           QString inAdditions,
           QObject *inParent = 0);

    int id() const;
    int subjectId() const;
    int typeId() const;
    int teacherId() const;
    int week() const;
    int dayOfWeek() const;
    int gridNumber() const;
    QString auditory() const;
    QString additions() const;

public slots:
    void setId(int arg);
    void setSubjectId(int arg);
    void setTypeId(int arg);
    void setTeacherId(int arg);
    void setWeek(int arg);
    void setDayOfWeek(int arg);
    void setGridNumber(int arg);
    void setAuditory(QString arg);
    void setAdditions(QString arg);

    static void dbCreateTable();
    void dbSave();
    void dbRemove();

private:
    int _id;
    int _subjectId;
    int _typeId;
    int _teacherId;
    int _week;
    int _dayOfWeek;
    int _gridNumber;
    QString _auditory;
    QString _additions;

    void clear();
};

#endif // ST_PAIR_H
