#include "st.teacher.h"

STTeacher::STTeacher(QObject *inParent) :
    QObject(inParent)
{
    this->clear();
}

STTeacher::STTeacher(int inId,
                     QString inName,
                     QString inNick,
                     QString inAdditions,
                     QObject *inParent) :
    QObject(inParent)
{
    _id = inId;
    _name = inName;
    _nick = inNick;
    _additions = inAdditions;
}

int STTeacher::id() const
{
    return _id;
}

QString STTeacher::name() const
{
    return _name;
}

QString STTeacher::nick() const
{
    return _nick;
}

QString STTeacher::additions() const
{
    return _additions;
}

void STTeacher::setId(int arg)
{
    if (_id != arg) {
        _id = arg;
        emit idChanged(arg);
    }
}

void STTeacher::setName(QString arg)
{
    if (_name != arg) {
        _name = arg;
        emit nameChanged(arg);
    }
}

void STTeacher::setNick(QString arg)
{
    if (_nick != arg) {
        _nick = arg;
        emit nickChanged(arg);
    }
}

void STTeacher::setAdditions(QString arg)
{
    if (_additions != arg) {
        _additions = arg;
        emit additionsChanged(arg);
    }
}

void STTeacher::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_TEACHER);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STTeacher::dbSave()
{
    QSqlQuery query;
    if (_id == DB_INVALID_ID) {
        query.prepare("insert into STTeacher values (NULL,?,?,?);");
        query.addBindValue(_name);
        query.addBindValue(_nick);
        query.addBindValue(_additions);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        setId( query.lastInsertId().toInt() );

        STDataProvider::dp()->objectSaved(this, true);
    } else {
        query.prepare("update STTeacher set name=?, nick=?, additions=? where id=?;");
        query.addBindValue(_name);
        query.addBindValue(_nick);
        query.addBindValue(_additions);
        query.addBindValue(_id);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        STDataProvider::dp()->objectSaved(this, false);
    }
}

void STTeacher::dbRemove()
{
    if (_id == DB_INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    QSqlQuery query;
    query.prepare("delete from STTeacher where id=?;");
    query.addBindValue(_id);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    STDataProvider::dp()->objectRemoved(this);
}

void STTeacher::clear()
{
    _id = DB_INVALID_ID;
    _name.clear();
    _nick.clear();
    _additions.clear();
}
