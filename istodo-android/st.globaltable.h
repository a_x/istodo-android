/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef ST_GLOBALTABLE_H
#define ST_GLOBALTABLE_H

#include "st.include.h"

class STGlobalTable : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int version READ version WRITE setVersion NOTIFY versionChanged)
    Q_PROPERTY(int pairLength READ pairLength WRITE setPairLength NOTIFY pairLengthChanged)
    Q_PROPERTY(QDate beginDate READ beginDate WRITE setBeginDate NOTIFY beginDateChanged)
    Q_PROPERTY(QDate endDate READ endDate WRITE setEndDate NOTIFY endDateChanged)

signals:
    void versionChanged(int arg);
    void pairLengthChanged(int arg);
    void beginDateChanged(QDate arg);
    void endDateChanged(QDate arg);

public:
    enum STGlobalTableEnum
    {
        db_version,
        db_pairLength,
        db_dBegin,
        db_mBegin,
        db_yBegin,
        db_dEnd,
        db_mEnd,
        db_yEnd
    };

    STGlobalTable(QObject *parent = 0);
    STGlobalTable(const STGlobalTable &inTable);
    STGlobalTable(int inVersion,
                  int inPairLength,
                  QDate inBeginDate,
                  QDate inEndDate,
                  QObject *parent = 0);

    STGlobalTable &operator=(const STGlobalTable &inTable);

    int version() const;
    int pairLength() const;
    QDate beginDate() const;
    QDate endDate() const;

public slots:
    void setVersion(int arg);
    void setPairLength(int arg);
    void setBeginDate(QDate arg);
    void setEndDate(QDate arg);

    static void dbCreateTable();
    void dbSave();
    void dbRemove();

private:
    int _version;
    int _pairLength;
    QDate _beginDate;
    QDate _endDate;

    void clear();
};

#endif // ST_GLOBALTABLE_H
