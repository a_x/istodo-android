/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef ST_TASK_H
#define ST_TASK_H

#include "st.include.h"

class STTask : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int subjectId READ subjectId WRITE setSubjectId NOTIFY subjectIdChanged)
    Q_PROPERTY(int seriesNumber READ seriesNumber WRITE setSeriesNumber NOTIFY seriesNumberChanged)
    Q_PROPERTY(bool isComplete READ isComplete WRITE setIsComplete NOTIFY isCompleteChanged)
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QString task READ task WRITE setTask NOTIFY taskChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(int prior READ prior WRITE setPrior NOTIFY priorChanged)

signals:
    void idChanged(int arg);
    void subjectIdChanged(int arg);
    void seriesNumberChanged(int arg);
    void isCompleteChanged(bool arg);
    void dateChanged(QDate arg);
    void taskChanged(QString arg);
    void descriptionChanged(QString arg);
    void priorChanged(int arg);

public:
    enum Priority
    {
        LOW,
        NORMAL,
        HIGH,
        ALARM
    };

    STTask(QObject *inParent = 0);
    STTask(int inId,
           int inSubjectId,
           int inSeriesNumber,
           bool inIsComplete,
           QDate inDate,
           QString inTask,
           QString inDescription,
           int inPrior,
           QObject *inParent = 0);

    int id() const;
    int subjectId() const;
    int seriesNumber() const;
    bool isComplete() const;
    QDate date() const;
    QString task() const;
    QString description() const;
    int prior() const;

    bool isOutdated();
public slots:
    void setId(int arg);
    void setSubjectId(int arg);
    void setSeriesNumber(int arg);
    void setIsComplete(bool arg);
    void setDate(QDate arg);
    void setTask(QString arg);
    void setDescription(QString arg);
    void setPrior(int arg);

    static void dbCreateTable();
    void dbSave();
    void dbRemove();

private:
    int _id;
    int _subjectId;
    int _seriesNumber;
    bool _isComplete;
    QDate _date;
    QString _task;
    QString _description;
    int _prior;

    void clear();
};

#endif // ST_TASK_H
