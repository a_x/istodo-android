#include "st.subject.h"

STSubject::STSubject(QObject *inParent) :
    QObject(inParent)
{
    this->clear();
}

STSubject::STSubject(int inId, QString inName, QString inNick, QObject *inParent) :
    QObject(inParent)
{
    _id = inId;
    _name = inName;
    _nick = inNick;
}

int STSubject::id() const
{
    return _id;
}

QString STSubject::name() const
{
    return _name;
}

QString STSubject::nick() const
{
    return _nick;
}

void STSubject::setId(int arg)
{
    if (_id != arg) {
        _id = arg;
        emit idChanged(arg);
    }
}

void STSubject::setName(QString arg)
{
    if (_name != arg) {
        _name = arg;
        emit nameChanged(arg);
    }
}

void STSubject::setNick(QString arg)
{
    if (_nick != arg) {
        _nick = arg;
        emit nickChanged(arg);
    }
}

void STSubject::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_SUBJECT);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STSubject::dbSave()
{
    QSqlQuery query;
    if (_id == DB_INVALID_ID) {
        query.prepare("insert into STSubject values (NULL,?,?);");
        query.addBindValue(_name);
        query.addBindValue(_nick);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        setId( query.lastInsertId().toInt() );

        STDataProvider::dp()->objectSaved(this, true);
    } else {
        query.prepare("update STSubject set name=?, nick=? where id=?;");
        query.addBindValue(_name);
        query.addBindValue(_nick);
        query.addBindValue(_id);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        STDataProvider::dp()->objectSaved(this, false);
    }
}

void STSubject::dbRemove()
{
    if (_id == DB_INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    QSqlQuery query;
    query.prepare("delete from STSubject where id=?;");
    query.addBindValue(_id);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    STDataProvider::dp()->objectRemoved(this);
}

void STSubject::clear()
{
    _id = DB_INVALID_ID;
    _name.clear();
    _nick.clear();
}
