/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef ST_DATAPROVIDER_H
#define ST_DATAPROVIDER_H

#include "st.include.h"

class STDataProvider : public QObject
{
    Q_OBJECT

    Q_PROPERTY(STDataProvider* qmlDp READ qmlDp WRITE setQmlDp NOTIFY qmlDpChanged)
    Q_PROPERTY(STGlobalTable globalTable READ globalTable NOTIFY globalTableChanged)
    Q_PROPERTY(QQmlListProperty<STBell> bells READ bells NOTIFY bellsChanged)
    Q_PROPERTY(QQmlListProperty<STPair> pairs READ pairs NOTIFY pairsChanged)
    Q_PROPERTY(QQmlListProperty<STPairType> pairTypes READ pairTypes NOTIFY pairTypesChanged)
    Q_PROPERTY(QQmlListProperty<STSubject> subjects READ subjects NOTIFY subjectsChanged)
    Q_PROPERTY(QQmlListProperty<STTask> tasks READ tasks NOTIFY tasksChanged)
    Q_PROPERTY(QQmlListProperty<STTeacher> teachers READ teachers NOTIFY teachersChanged)
    Q_PROPERTY(QDate currentDate READ currentDate WRITE setCurrentDate NOTIFY currentDateChanged)

signals:
    void qmlDpChanged(STDataProvider* arg);
    void globalTableChanged(STGlobalTable arg);
    void bellsChanged(QQmlListProperty<STBell> arg);
    void pairsChanged(QQmlListProperty<STPair> arg);
    void pairTypesChanged(QQmlListProperty<STPairType> arg);
    void subjectsChanged(QQmlListProperty<STSubject> arg);
    void tasksChanged(QQmlListProperty<STTask> arg);
    void teachersChanged(QQmlListProperty<STTeacher> arg);
    void currentDateChanged(QDate arg);

public:
    STDataProvider(QString inFileName = QString(), QObject *parent = 0);

    Q_INVOKABLE QString getDatabaseName();

    void loadFromFile(QString inFileName);
    void importFromFile(QString fileName);

    static QVariant getVariantFromId(int inId);
    static int getIdFromVariant(QVariant inVar);

    static STDataProvider *dp();

    STDataProvider *qmlDp();
    STGlobalTable globalTable() const;
    QQmlListProperty<STBell> bells();
    QQmlListProperty<STPair> pairs();
    QQmlListProperty<STPairType> pairTypes();
    QQmlListProperty<STSubject> subjects();
    QQmlListProperty<STTask> tasks();
    QQmlListProperty<STTeacher> teachers();
    Q_INVOKABLE QString shortDayName(int day)const;
    Q_INVOKABLE QString shortMonthName(int month)const;
    Q_INVOKABLE QDate currentDate() const;
    Q_INVOKABLE bool isEmpty();
    Q_INVOKABLE void addDays(int count);
    Q_INVOKABLE int getCountOfWeeks() const;
    Q_INVOKABLE int getWeekNumber() const;
    Q_INVOKABLE int getWeekSemestrNumber() const;
    Q_INVOKABLE STBell* getBell(int num);
    Q_INVOKABLE STPair* getPair(int id);
    Q_INVOKABLE STPairType* getPairType(int id);
    Q_INVOKABLE STSubject* getSubject(int id);
    Q_INVOKABLE STTask* getTask(int id);
    Q_INVOKABLE STTeacher* getTeacher(int id);

public slots:
    void setQmlDp(STDataProvider* arg);
    void setCurrentDate(QDate arg);

    void objectSaved(QObject *obj, bool newObj = false);
    void objectRemoved(QObject *obj);

private:
    STGlobalTable *_globalTable;
    QList<STBell*> _bells;
    QList<STPair*> _pairs;
    QList<STPairType*> _pairTypes;
    QList<STSubject*> _subjects;
    QList<STTask*> _tasks;
    QList<STTeacher*> _teachers;

    void refillAll();
    void refillGlobalTable();
    void refillBells();
    void refillPairs();
    void refillPairTypes();
    void refillSubjects();
    void refillTasks();
    void refillTeachers();
    QQmlListProperty<STBell> m_bells;
    QDate _currentDate;
    bool _firstLaunch;
};

#endif // ST_DATAPROVIDER_H
