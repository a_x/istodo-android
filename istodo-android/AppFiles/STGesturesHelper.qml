/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.1

import ru.istodo.istodo 1.0
import "../ActionBar"
import "../Controls"
import "../Global"

Rectangle {
    id: root

    property bool needConfirm: false
    property int size: Math.min(root.height, root.width) - 1*mm

    ACActionBar {
        id: topBar
        width: parent.width
        height: ACGlobal.headerHeight
        anchors.top: root.top

        firstColor: "#535353"
        secondColor: "#333"

        leftComponent: rectLeft
    }

    Component {
        id: rectLeft
        ACNavDrawerButton {
            id: pbNavDriver
            width: ACGlobal.headerHeight*3;
            viewName: qsTr("Жесты")
            viewInfo: "Swipe"
        }
    }

    Text {
        id: lbTitle
        color: ACGlobal.style.holoDarkBlue
        font.pixelSize: 3*mm
        text:"Переключение дней"
        anchors {
            top: topBar.bottom
            topMargin: 1*mm
            horizontalCenter: root.horizontalCenter
        }
    }

    Image{
        id: swipeLeft

        sourceSize.width: size/3
        sourceSize.height: size/3
        source: "qrc:/img/images/other/swipeLeft.svg"
        anchors {
            top: lbTitle.bottom
            topMargin: 2*mm
            left: root.left
            leftMargin: 5*mm
        }
    }

    Image{
        id: swipeRight

        sourceSize.width: size/3
        sourceSize.height: size/3
        source: "qrc:/img/images/other/swipeRight.svg"
        anchors {
            top: lbTitle.bottom
            topMargin: 2*mm
            right: root.right
            rightMargin: 5*mm
        }
    }

    Text {
        id: lbLeft
        color: ACGlobal.style.holoDarkBlue
        font.pixelSize: 2.5*mm
        text:"Назад"
        anchors {
            top: swipeLeft.bottom
            topMargin: -10*mm
            right: swipeLeft.right
            rightMargin: 1*mm
        }
    }

    Text {
        id: lbRight
        color: ACGlobal.style.holoDarkBlue
        font.pixelSize: 2.5*mm
        text:"Вперед"
        anchors {
            top: swipeRight.bottom
            topMargin: -10*mm
            left: swipeRight.left
            leftMargin: 1*mm
        }
    }

    Text {
        id: lbCenter
        color: ACGlobal.style.holoDarkBlue
        width: size
        font.pixelSize: 2.3*mm
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text:"или короткий горизонтальный <b>swipe</b>"
        anchors {
            top: swipeRight.bottom
            topMargin: 3*mm
            horizontalCenter: root.horizontalCenter
        }
    }
}
