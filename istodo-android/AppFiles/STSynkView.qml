/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.1

import ru.istodo.istodo 1.0
import "../ActionBar"
import "../Controls"
import "../Global"

Rectangle {
    id: root

    property bool needConfirm: false
    property int size: Math.min(root.height, root.width) - 1*mm

    ACActionBar {
        id: topBar
        width: parent.width
        height: ACGlobal.headerHeight
        anchors.top: root.top

        firstColor: "#535353"
        secondColor: "#333"

        leftComponent: rectLeft
    }

    Component {
        id: rectLeft
        ACNavDrawerButton {
            id: pbNavDriver
            width: ACGlobal.headerHeight*3;
            viewName: qsTr("Синхронизация")
            viewInfo: "Dropbox"
        }
    }

    Image{
        id: dbLogo

        sourceSize.width: size/2
        sourceSize.height: size/2
        source: "qrc:/img/images/other/dropBoxShort.svg"
        anchors {
            top: topBar.bottom
            horizontalCenter: root.horizontalCenter
        }
    }

    Connections {
        target: ACGlobal.dropbox

        onNeedConfirmOnSite: {
            needConfirm = true
            dbSync.text = qsTr("Продолжить")
            lbStatus.text = qsTr("Необходимо разрешить iStodo синхронизацию")
        }

        onImportFinished: {
            needConfirm = false
            lbStatus.text = qsTr("Импорт успешно завершен!")
            dbSync.text = qsTr("Импортировать")
        }

        onError: {
            lbStatus.text = qsTr("Ошибка загрузки данных!")
            errorToast.showToast()
        }
    }

    ACToast {
        id: errorToast
        toastText: qsTr("Проверьте подключение к сети")
    }

    ACButton {
        id: dbSync
        anchors.centerIn: parent
        text: qsTr("Импортировать")
        onClicked: {

            if (needConfirm) {
                ACGlobal.dropbox.continueProcess()
                lbStatus.text = qsTr("Импортирую базу данных...")
            }
            else {
                lbStatus.text = qsTr("Импортирую базу данных...")
                ACGlobal.dropbox.startImport()
                // Incredible kludge for android
                lbStatus.text = qsTr("Готово!")
            }
        }
    }

    Text {
        id: lbStatus
        color: "#222"
        height: 10*mm
        width: size
        font.pixelSize: 3.5*mm
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        text:""
        anchors {
            bottom: root.bottom
            bottomMargin: 5*mm
            left: root.left
            leftMargin: 2*mm
        }
    }
}
