/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import ru.istodo.istodo 1.0
import "../ActionBar"
import "../Controls"
import "../Dialogs"
import "../Global"
import "../Other"

Rectangle {
    id: root

    property date today: new Date()


    ACActionBar {
        id: topBar
        width: parent.width
        height: ACGlobal.headerHeight
        anchors.top: parent.top

        firstColor: "#535353"
        secondColor: "#333"

        leftComponent: rectLeft
        rightComponent: rectRight
    }

    Component {
        id: rectLeft
        ACNavDrawerButton {
            id: pbNavDriver
            width: ACGlobal.headerHeight*3;
            viewName: qsTr("День")
            viewInfo: Qt.formatDate(ACGlobal.dp.currentDate, "dd MMMM yyyy")
        }
    }

    Component {
        id: rectRight
        ACCalendarButton {
            id: pbCalendar
            imgSize: 5*mm
            text: Qt.formatDate(ACGlobal.dp.currentDate, "dd")
            onClicked: {
                datePicker.showDialog()
            }
        }
    }

    TabView {
        id: tabBar

        width: parent.width
        height: parent.height - topBar.height
        anchors.top: topBar.bottom

        Tab {
            title: qsTr("РАСПИСАНИЕ")
            component: STDaySchedule {
            }
        }
        Tab {
            title: qsTr("ЗАДАЧИ")
            component: STDayTasks {
            }
        }
        style: ACGlobal.style.tvTwoTabStyle
    }

    ACDatePicker {
        id: datePicker

        z: 10
        day: Qt.formatDateTime( today, "d" )
        month: Qt.formatDateTime( today, "M" )
        year: Qt.formatDateTime( today, "yyyy" )
        onDateSelected: {

            var buffDate = new Date()
            buffDate.setDate(day)
            buffDate.setMonth(month)
            buffDate.setFullYear(year)
            //console.log(day + " " + month + " "+ year)
            //console.log( Qt.formatDateTime( buffDate, "dd.MM.yyyy" ) )
            ACGlobal.dp.setCurrentDate(buffDate)
        }
    }
}
