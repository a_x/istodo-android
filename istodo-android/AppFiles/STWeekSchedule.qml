/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import ru.istodo.istodo 1.0
import "../Controls"
import "../Dialogs"
import "../Global"

Rectangle {

    ListView {
        id: scheduleList

        anchors.fill: parent

        clip: true
        model: ACGlobal.dp.pairs
        boundsBehavior: Flickable.StopAtBounds

        delegate: Item {
            id: item
            property string subjColor: "#fff"

            height: 4*mm

            Row {
                id: row

                spacing: 0

                anchors {
                    left: parent.left
                    leftMargin: 1*mm
                    verticalCenter: parent.verticalCenter
                }

                Rectangle {
                    id: timeRect

                    width: 7*mm
                    height: 3*mm
                    color: {
                        var buffSubj = ACGlobal.dp.getSubject(subjectId);
                        if(buffSubj === undefined) {
                            item.subjColor = "orange"
                        }
                        else{
                            item.subjColor = ACGlobal.style.colorModel.get(subjectId%10).color
                        }
                        item.subjColor
                    }

                    Text {
                        color: "#fff"
                        font.pixelSize: 2*mm
                        text: {
                            var buffTime = ACGlobal.dp.getBell(gridNumber).time
                            Qt.formatDateTime ( buffTime, "hh:mm" )
                        }
                        anchors.centerIn: parent
                    }
                }

                Text {
                    id: subject

                    color: "#111"
                    font.bold: true
                    font.pixelSize: 2*mm
                    text: " " + ACGlobal.dp.getSubject(subjectId).nick
                }

                Text {
                    id: auditLabel
                    color: "#555"
                    font.pixelSize: 2*mm
                    text: " в " + auditory
                }

                Text {
                    color: item.subjColor
                    font.pixelSize: 2*mm
                    text: ", " + ACGlobal.dp.getPairType(typeId).shortName
                }
            }

            //                Rectangle {
            //                    width: 12
            //                    height: 12
            //                    color: ACGlobal.style.colorModel.get(subjectId%10).color

            //            Text {
            //                color: "#fff"
            //                font.pointSize: 12
            //                anchors.centerIn: parent
            //                text: ACGlobal.dp.getPairType(typeId).shortName
            //            }
            //                }

            //                Text {
            //                    color: "#333"
            //                    font.pointSize: 12
            //                    text: auditory + " / " + additions
            //                }

            Rectangle { // Bottom border
                width: scheduleList.width
                height: 0.2*mm
                color: "#dcdcdc"
                anchors.bottom: parent.bottom
            }
        }
    }

    Component.onCompleted: {
        ListView.update()
    }
}
