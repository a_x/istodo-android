/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.1
import "../Global"
import "../Other"
import "../Controls"

Rectangle {

    ListModel {
        id: dataModel

        Component.onCompleted: {

            var value
            value = {
                name: "День",
                view: dayView
            }
            append(value)

            value = {
                name: "Неделя",
                view: weekView
            }
            append(value)

            value = {
                name: "Синхронизация",
                view: synkView
            }
            append(value)

            value = {
                name: "Жесты",
                view: gstHelper
            }
            append(value)

            stackView.push({item:dataModel.get(0).view,immediate: true})
        }
    }

    ACStackView {
        id: stackView

        Component.onCompleted: {
            ACGlobal.stack = stackView
        }
    }

    Rectangle {
        focus: true // important - otherwise we'll get no key events

        Keys.onReleased: {
            if (event.key == Qt.Key_Back) {
                if(ACGlobal.stack.depth > 1) {
                    ACGlobal.stack.pop()
                    event.accepted = true
                }
            }
        }
    }

    ACNavigationDrawer {
        id: navDrawer

        y: ACGlobal.headerHeight

        width: parent.width
        height: parent.height - ACGlobal.headerHeight

        model:dataModel

        onViewSelected: {
            ACGlobal.stack.clear()
            ACGlobal.stack.push({item:dataModel.get(inNumber).view,immediate: true})
        }

        Component.onCompleted: {
            ACGlobal.navDrawer = navDrawer
        }
    }

    STDay {
        id: dayView
        visible: false
    }

    STWeek  {
        id: weekView
        visible: false
    }

    STSynkView  {
        id: synkView
        visible: false
    }

    STGesturesHelper {
        id: gstHelper
        visible: false
    }
}
