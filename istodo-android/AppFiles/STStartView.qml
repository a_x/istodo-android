/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.0
import "../Controls"
import "../Global"

Rectangle {
    id: root

    // Rewrite this view to states
    property bool needConfirm: false
    property int size: Math.min(root.height, root.width) - 1*mm

    Image{
        id: dbLogo

        sourceSize.width: size/1.5
        sourceSize.height: size/1.5
        source: "qrc:/img/images/other/dropBoxShort.svg"
        anchors {
            top: root.top
            horizontalCenter: root.horizontalCenter
        }
    }

//    Text {
//        id: lbDropRequired
//        color: "#222"
//        width: size
//        font.pixelSize: 3.5*mm
//        wrapMode: Text.WordWrap
//        horizontalAlignment: Text.AlignHCenter
//        text:"Для работы програмы нужно установить приложение <a href=\"Http://dropbox.com\">Dropbox</a>..."

//        anchors {
//            top: dbLogo.bottom
//            horizontalCenter: root.horizontalCenter
//        }

//        onLinkActivated: {
//            Qt.openUrlExternally("https://play.google.com/store/apps/details?id=com.dropbox.android")
//        }
//    }

    Text {
        id: lbInstruction
        color: "#222"
        width: size
        font.pixelSize: 3.5*mm
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text:"Для начала работы нужно разрешить синхронизацию:"

        anchors {
            top: dbLogo.bottom
            horizontalCenter: root.horizontalCenter
        }
    }

    ACButton {
        id: synkButton
        text: "Разрешить"
        anchors {
            top: lbInstruction.bottom
            topMargin: 5*mm
            horizontalCenter: root.horizontalCenter
        }

        onClicked: {
            if (needConfirm) {
                ACGlobal.dropbox.continueProcess()
                lbStatus.text = qsTr("Импортирую базу данных...")
            }
            else {
                ACGlobal.dropbox.startImport()
                lbStatus.text = qsTr("Импортирую базу данных...")
            }
        }

    }

    Text {
        id: lbStatus
        color: "#222"
        height: 10*mm
        width: size - imgBack.width - 5*mm
        font.pixelSize: 3.5*mm
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        text:"А потом вернуться назад из браузера"
        anchors {
            bottom: root.bottom
            bottomMargin: 5*mm
            left: root.left
            leftMargin: 2*mm
        }
    }

    Image{
        id: imgBack

        sourceSize.width: 10*mm
        sourceSize.height: 10*mm
        source: "qrc:/img/images/other/back.svg"
        anchors {
            bottom: root.bottom
            bottomMargin: 5*mm
            right: root.right
            rightMargin: 2*mm
        }
        visible: !needConfirm
    }

    Connections {
        target: ACGlobal.dropbox

        onNeedConfirmOnSite: {
            needConfirm = true
            lbStatus.text = ""
            synkButton.text = qsTr("Продолжить")
        }

        onImportFinished: {
            root.visible = false
        }

        onError: {
            lbStatus.text = qsTr("Ошибка! Перезапустите приложение и попробуйте снова )=")
            imgBack.visible = true
        }
    }
}
