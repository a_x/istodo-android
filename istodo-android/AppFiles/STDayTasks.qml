/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import ru.istodo.istodo 1.0
import "../Controls"
import "../Dialogs"
import "../Global"
import "../Other"

Rectangle {

    ListModel {
        id: tasksModel
    }

    ListView {
        id: taskList

        anchors.fill: parent

        clip: true
        model: tasksModel
        boundsBehavior: Flickable.StopAtBounds

        delegate: Item {
            height: 8*mm

            Row {
                id: row

                spacing: 1*mm
                anchors {
                    left: parent.left
                    leftMargin: 1*mm
                    verticalCenter: parent.verticalCenter
                }

                Rectangle { //Left square
                    width: 6*mm
                    height: 6*mm
                    color: {
                        var buffSubj = ACGlobal.dp.getSubject(subjectId);
                        if(buffSubj === undefined) {
                            "orange"
                        }
                        else{
                            ACGlobal.style.colorModel.get(subjectId%10).color
                        }
                    }

                    Image {
                        source: {
                            switch (prior) {
                            case 0:
                                "qrc:/img/images/icons/lightPlane.svg"
                                break
                            case 1:
                                "qrc:/img/images/icons/plane.svg"
                                break
                            case 2:
                                "qrc:/img/images/icons/rocket.svg"
                                break
                            case 3:
                                "qrc:/img/images/icons/radiation.svg"
                                break
                            }

                        }
                        sourceSize.height: 4.5*mm
                        sourceSize.width: 4.5*mm
                        anchors.centerIn: parent
                    }

                    //                    MouseArea {
                    //                        anchors.fill: parent
                    //                        onClicked: {
                    //                            taskEditor.setTask(id)
                    //                            ACGlobal.stack.push(taskEditor)
                    //                        }
                    //                    }
                }

                Column {

                    Text {
                        color: "#111"
                        font.pixelSize: 3*mm
                        text: {
                            var buffSubj = ACGlobal.dp.getSubject(subjectId);
                            if(buffSubj === undefined) {
                                return qsTr("Общее")
                            }
                            else{
                                return ACGlobal.dp.getSubject(subjectId).nick
                            }
                        }
                    }
                    Text {
                        id: taskText

                        color: "#222"
                        font.pixelSize: 2.3*mm
                        text: task
                        elide: Text.ElideRight
                        width: taskList.width - 10*mm

                    }
                }
            }

            Rectangle { // Bottom border
                width: taskList.width
                height: parseInt(0.2*mm)
                color: "#dcdcdc"
                anchors.bottom: parent.bottom
            }
        }
        ACSwipeArea {
            id: mouse
            anchors.fill: parent
            onSwipe: {
                switch (direction) {
                case "left":
                    ACGlobal.dp.addDays(-1)
                    break
                case "right":
                    ACGlobal.dp.addDays(+1)
                    break
                }
            }
        }
    }

    function updateModel() {
        tasksModel.clear()

        var currDate = ACGlobal.dp.currentDate;
        for(var i=0; i<ACGlobal.dp.tasks.length; ++i)
        {
            var buffElm = ACGlobal.dp.tasks[i]
            // Check date
            if(buffElm.date.toString() === ACGlobal.dp.currentDate.toString() ) {
                tasksModel.append(buffElm)
            }
        }
    }

    Connections {
        target: ACGlobal.dp

        onTasksChanged: {
            updateModel()
        }
    }

    Text {
        id: stub
        text: qsTr("Заданий на этот день нет")
        font.pixelSize: 3*mm
        color: "#777"
        anchors.centerIn: parent
        visible: !tasksModel.count
    }

    Component.onCompleted: {
        updateModel()
    }
}
