/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.1

import ru.istodo.istodo 1.0
import "../Global"
import "../Other"

ApplicationWindow {
    visible: true
    width: 370
    height: 500

    title: qsTr("iStodo Android")

    ACStyle {
        id: style

        Component.onCompleted: {
            ACGlobal.style = style
        }
    }

    STDropbox {
        id: dropbox

        Component.onCompleted: {
            ACGlobal.dropbox = dropbox
        }
    }

    STDataProvider {
        id: provider

        Component.onCompleted: {
            ACGlobal.dp = provider
            provider.qmlDp = provider
            //startView.visible = provider.isEmpty()
            console.log(provider.isEmpty())
            console.log("dp is ", ACGlobal.dp)
        }
    }

    ACNavigationController {
        id: navCtrl
        anchors.fill: parent
    }

    ACSwipeArea {
        id: mouse
        width: 3*mm
        height: parent.height - ACGlobal.headerHeight*2
        anchors {
            left: parent.left
            topMargin: ACGlobal.headerHeight*2
        }
        onSwipe: {
            switch (direction) {
            case "right":
                ACGlobal.navDrawer.showDrawer()
                break
            }
        }
    }

    STStartView {
        id: startView
        anchors.fill: parent
        visible: false
    }

}
