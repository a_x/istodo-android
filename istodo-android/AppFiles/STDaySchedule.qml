/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1


import ru.istodo.istodo 1.0
import "../Controls"
import "../Global"
import "../Other"

Rectangle {

    function pairsComparator(a, b){
        if(a.gridNumber < b.gridNumber)
            return -1
        if(a.gridNumber > b.gridNumber)
            return 1
        return 0
    }

    function updateModel() {
        pairsModel.clear()
        var dayPairs = []

        var currDate = ACGlobal.dp.currentDate;

        for(var i=0; i<ACGlobal.dp.pairs.length; i++) {
            var buffElm = ACGlobal.dp.pairs[i]
            // Check week
            if(buffElm.week === ACGlobal.dp.getWeekNumber() ) {
                // Check day of week
                if(buffElm.dayOfWeek === currDate.getDay() ) {
                    dayPairs.push(buffElm)
                }
            }
        }

        dayPairs.sort(pairsComparator)
        for(var i = 0; i < dayPairs.length; i++) {
            pairsModel.append(dayPairs[i])
        }
    }

    ListModel {
        id: pairsModel
    }

    ListView {
        id: scheduleList

        anchors.fill: parent

        clip: true
        model: pairsModel
        boundsBehavior: Flickable.StopAtBounds

        delegate: Item {
            id: item
            property string subjColor: "#fff"

            height: 8*mm

            Row {
                id: row

                spacing: 1*mm
                anchors {
                    left: parent.left
                    leftMargin: 1*mm
                    verticalCenter: parent.verticalCenter
                }

                Rectangle {
                    id: timeRect

                    width: 6*mm
                    height: 6*mm
                    color: {
                        var buffSubj = ACGlobal.dp.getSubject(subjectId);
                        if(buffSubj === undefined) {
                            item.subjColor = "orange"
                        }
                        else{
                            item.subjColor = ACGlobal.style.colorModel.get(subjectId%10).color
                        }
                        item.subjColor
                    }

                    Text {
                        color: "#fff"
                        font.pixelSize: 2*mm
                        text: {
                            var buffTime = ACGlobal.dp.getBell(gridNumber).time
                            Qt.formatDateTime ( buffTime, "hh:mm" )
                        }
                        anchors.centerIn: parent
                    }
                }

                Column {
                    Row {
                        Text {
                            color: "#111"
                            font.pixelSize: 2.5*mm
                            text: ACGlobal.dp.getSubject(subjectId).nick
                            elide: Text.ElideRight
                            //width: scheduleList.width - auditLabel.paintedWidth - timeRect.width - 2*mm
                        }

                        Text {
                            id: auditLabel
                            color: "#555"
                            font.pixelSize: 2*mm
                            text: "в " + auditory
                            anchors.bottom: parent.bottom
                        }

                        spacing: 1*mm
                    }

                    Row {
                        spacing: 1*mm

                        Text {
                            color: "#333"
                            font.pixelSize: 2*mm
                            text: ACGlobal.dp.getTeacher(teacherId).nick + ","
                        }

                        Text {
                            color: item.subjColor
                            font.pixelSize: 2*mm
                            text: additions
                            anchors.bottom: parent.bottom
                        }

                        Text {
                            font.pixelSize: 2*mm
                            text: "— " + ACGlobal.dp.getPairType(typeId).shortName
                        }
                    }
                }
            }

            Rectangle { // Bottom border
                width: scheduleList.width
                height: parseInt(0.2*mm)
                color: "#dcdcdc"
                anchors.bottom: parent.bottom
            }
        }

        ACSwipeArea {
            id: mouse
            anchors.fill: parent
            onSwipe: {
                switch (direction) {
                case "left":
                    ACGlobal.dp.addDays(-1)
                    break
                case "right":
                    ACGlobal.dp.addDays(+1)
                    break
                }
            }
        }
    }

    Text {
        id: stub
        text: qsTr("Занятий в этот день нет")
        font.pixelSize: 3*mm
        color: "#777"
        anchors.centerIn: parent
        visible: !pairsModel.count
    }

    // Epic Kludge
    Timer {
        id: dpTimer
        interval: 1; running: true;
        onTriggered: {
            updateModel()
        }
    }

    Connections {
        id: dpConnect
        target: ACGlobal.dp

        onPairsChanged: {
            updateModel()
        }
    }
}
