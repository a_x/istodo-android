#include "st.pairtype.h"

STPairType::STPairType(QObject *inParent) :
    QObject(inParent)
{
    this->clear();
}

STPairType::STPairType(int inId,
                       QString inFullName,
                       QString inShortName,
                       QObject *inParent) :
    QObject(inParent)
{
    _id = inId;
    _fullName = inFullName;
    _shortName = inShortName;
}

int STPairType::id() const
{
    return _id;
}

QString STPairType::fullName() const
{
    return _fullName;
}

QString STPairType::shortName() const
{
    return _shortName;
}

void STPairType::setId(int arg)
{
    if (_id != arg) {
        _id = arg;
        emit idChanged(arg);
    }
}

void STPairType::setFullName(QString arg)
{
    if (_fullName != arg) {
        _fullName = arg;
        emit fullNameChanged(arg);
    }
}

void STPairType::setShortName(QString arg)
{
    if (_shortName != arg) {
        _shortName = arg;
        emit shortNameChanged(arg);
    }
}

void STPairType::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_PAIRTYPE);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STPairType::dbSave()
{
    QSqlQuery query;
    if (_id == DB_INVALID_ID) {
        query.prepare("insert into STPairType values (NULL,?,?);");
        query.addBindValue(_fullName);
        query.addBindValue(_shortName);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        setId( query.lastInsertId().toInt() );

        STDataProvider::dp()->objectSaved(this, true);
    } else {
        query.prepare("update STPairType set fullName=?, shortName=? where id=?;");
        query.addBindValue(_fullName);
        query.addBindValue(_shortName);
        query.addBindValue(_id);
        if (!query.exec()) {
            DB_QUERY_ERROR;
        }

        STDataProvider::dp()->objectSaved(this, false);
    }
}

void STPairType::dbRemove()
{
    if (_id == DB_INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    QSqlQuery query;
    query.prepare("delete from STPairType where id=?;");
    query.addBindValue(_id);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    STDataProvider::dp()->objectRemoved(this);
}

void STPairType::clear()
{
    _id = DB_INVALID_ID;
    _fullName.clear();
    _shortName.clear();
}
