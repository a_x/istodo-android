/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.0
import "../Global"

Rectangle {
    property alias model: pickerList.model

    signal indexSelected(int value)

    function setValue(value) {
        pickerList.setValue(value)
    }

    width: 10*mm
    height: 25*mm

    ACPickerList {
        id: pickerList

        width: parent.width
        height: parent.height

        onIndexChanged: {
            indexSelected(value)
        }
    }

    Image {
        id: upShadow

        sourceSize.height: 10*mm
        sourceSize.width: 10*mm
        source: "qrc:/img/images/icons/pickerShadowUp.svg"
        anchors {
            top: parent.top
        }
    }

    Image {
        id: downShadow

        sourceSize.height: 10*mm
        sourceSize.width: 10*mm
        source: "qrc:/img/images/icons/pickerShadowDown.svg"
        anchors {
            bottom: parent.bottom
        }
    }

    Rectangle {
        id: topSelector

        width: parent.width
        height: parseInt(0.3*mm)
        color: ACGlobal.style.holoLightBlue

        anchors {
            top: parent.top
            topMargin: pickerList.itemHeight
        }
    }

    Rectangle {
        id: bottomSelector

        width: parent.width
        height: parseInt(0.3*mm)
        color: ACGlobal.style.holoLightBlue

        anchors {
            top: parent.top
            topMargin: pickerList.itemHeight*2
        }
    }
}
