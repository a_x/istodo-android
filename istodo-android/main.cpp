#include <QDebug>
#include <QScreen>
#include <QQmlContext>
#include <QApplication>
#include <QQmlApplicationEngine>

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#endif

#include "st.dropbox.h"
#include "st.dataprovider.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setApplicationName("iStodo");

     qSetMessagePattern("[%{file}:%{line}] %{type}: %{message}");

#ifdef Q_OS_ANDROID
    //  BUG with dpi on some androids: https://bugreports.qt-project.org/browse/QTBUG-35701
    //  Workaround:
    QAndroidJniObject qtActivity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    QAndroidJniObject resources = qtActivity.callObjectMethod("getResources", "()Landroid/content/res/Resources;");
    QAndroidJniObject displayMetrics = resources.callObjectMethod("getDisplayMetrics", "()Landroid/util/DisplayMetrics;");
    int density = displayMetrics.getField<int>("densityDpi");
#else
    QScreen *screen = qApp->primaryScreen();
    float density = screen->physicalDotsPerInch() * 2;
#endif

     // Регистрируем типы для моделей QML.
     qmlRegisterType<STDropbox>("ru.istodo.istodo", 1, 0, "STDropbox");
     qmlRegisterType<STDataProvider>("ru.istodo.istodo", 1, 0, "STDataProvider");
     qmlRegisterType<STGlobalTable>("ru.istodo.istodo", 1, 0, "STGlobalTable");
     qmlRegisterType<STBell>("ru.istodo.istodo", 1, 0, "STBell");
     qmlRegisterType<STPair>("ru.istodo.istodo", 1, 0, "STPair");
     qmlRegisterType<STPairType>("ru.istodo.istodo", 1, 0, "STPairType");
     qmlRegisterType<STSubject>("ru.istodo.istodo", 1, 0, "STSubject");
     qmlRegisterType<STTask>("ru.istodo.istodo", 1, 0, "STTask");
     qmlRegisterType<STTeacher>("ru.istodo.istodo", 1, 0, "STTeacher");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("mm",density / 25.4);
    engine.load(QUrl(QStringLiteral("qrc:/AppFiles/main.qml")));

    return app.exec();
}
