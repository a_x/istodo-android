/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef ST_INCLUDE_H
#define ST_INCLUDE_H

#include <QtCore>
#include <QtSql>
#include <QtNetwork>
#include <QDesktopServices>
#include <QtQml>


// Dataprovider
#include "st.script.h"

class STDataProvider;
class STGlobalTable;
class STBell;
class STPair;
class STPairType;
class STSubject;
class STTask;
class STTeacher;

#include "st.dataprovider.h"
#include "st.globaltable.h"
#include "st.bell.h"
#include "st.pair.h"
#include "st.pairtype.h"
#include "st.subject.h"
#include "st.task.h"
#include "st.teacher.h"

// Database
#define DB_NAME "iStodo.db"
#define DB_VERSION 1
#define DB_QUERY_ERROR qCritical( "%s", query.lastError().text().toStdString().c_str() )
#define DB_INVALID_ID -1

// Settings
#define SET_TOKEN_SECRET "token/secret"
#define SET_TOKEN "token/token"

// Version
#define ST_BASIC_VERSION 1200

#ifdef Q_OS_WIN32
#define ST_VERSION ST_BASIC_VERSION+1
#endif

#ifdef Q_OS_LINUX
#define ST_VERSION ST_BASIC_VERSION+2
#endif

#ifdef Q_OS_MAC
#define ST_VERSION ST_BASIC_VERSION+3
#endif

#define ST_STR_VERSION QString("%1.%2.%3")\
    .arg((ST_VERSION)/1000%10)\
    .arg((ST_VERSION)/100%10)\
    .arg((ST_VERSION)/10%10)

#endif // ST_INCLUDE_H
