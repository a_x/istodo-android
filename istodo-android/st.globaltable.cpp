#include "st.globaltable.h"

STGlobalTable::STGlobalTable(QObject *parent) :
    QObject(parent)
{
    this->clear();
}

STGlobalTable::STGlobalTable(const STGlobalTable &inTable) :
    QObject(inTable.parent())
{
    _version = inTable._version;
    _pairLength = inTable._pairLength;
    _beginDate = inTable._beginDate;
    _endDate = inTable._endDate;
}

STGlobalTable::STGlobalTable(int inVersion,
                             int inPairLength,
                             QDate inBeginDate,
                             QDate inEndDate,
                             QObject *parent) :
    QObject(parent)
{
    _version = inVersion;
    _pairLength = inPairLength;
    _beginDate = inBeginDate;
    _endDate = inEndDate;
}

STGlobalTable &STGlobalTable::operator=(const STGlobalTable &inTable)
{
    //проверка на самоприсваивание
    if (this == &inTable) {
        return *this;
    }

    _version = inTable._version;
    _pairLength = inTable._pairLength;
    _beginDate = inTable._beginDate;
    _endDate = inTable._endDate;
    return *this;
}

int STGlobalTable::version() const
{
    return _version;
}

int STGlobalTable::pairLength() const
{
    return _pairLength;
}

QDate STGlobalTable::beginDate() const
{
    return _beginDate;
}

QDate STGlobalTable::endDate() const
{
    return _endDate;
}

void STGlobalTable::setVersion(int arg)
{
    if (_version != arg) {
        _version = arg;
        emit versionChanged(arg);
    }
}

void STGlobalTable::setPairLength(int arg)
{
    if (_pairLength != arg) {
        _pairLength = arg;
        emit pairLengthChanged(arg);
    }
}

void STGlobalTable::setBeginDate(QDate arg)
{
    if (_beginDate != arg) {
        _beginDate = arg;
        emit beginDateChanged(arg);
    }
}

void STGlobalTable::setEndDate(QDate arg)
{
    if (_endDate != arg) {
        _endDate = arg;
        emit endDateChanged(arg);
    }
}

void STGlobalTable::dbCreateTable()
{
    QSqlQuery query;
    query.prepare(DB_CREATE_GLOBAL);
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }
}

void STGlobalTable::dbSave()
{
    QSqlQuery query;
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_version);
    query.addBindValue(_version);
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_pairLength);
    query.addBindValue(_pairLength);
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    // Insert begin date
    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_dBegin);
    query.addBindValue(_beginDate.day());
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_mBegin);
    query.addBindValue(_beginDate.month());
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_yBegin);
    query.addBindValue(_beginDate.year());
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    // Insert end date
    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_dEnd);
    query.addBindValue(_endDate.day());
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_mEnd);
    query.addBindValue(_endDate.month());
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(db_yEnd);
    query.addBindValue(_endDate.year());
    if( !query.exec() ) {
        DB_QUERY_ERROR;
    }
}

void STGlobalTable::dbRemove()
{
    QSqlQuery query;
    query.prepare("delete from STGlobal;");
    if (!query.exec()) {
        DB_QUERY_ERROR;
    }

    // NOTE: clear change fields, but doesn't emits any signals
    this->clear();

    // TODO: notify dataprovider
}

void STGlobalTable::clear()
{
    _version = -1;
    _pairLength = -1;
    _beginDate = QDate();
    _endDate = QDate();
}
