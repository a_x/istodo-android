/*******************************************************************************
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** 3. Neither the name of the copyright holder nor the names of its contributors
** may be used to endorse or promote products derived from this software without
** specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
** IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.0
import "../Global"

Rectangle {
    id: root
    property alias viewName: viewName.text
    property alias viewInfo: viewInfo.text

    color: "transparent"

    Image {
        id: drawerIcon

        sourceSize.width: 5*mm
        sourceSize.height: 5*mm

        state: "hideDrawer"
        anchors{
            left: parent.left
            leftMargin: -2*mm
            verticalCenter: parent.verticalCenter
        }
        source: "qrc:/img/images/icons/navDrawerIcon.svg"

        states: [
            State {
                name: "hideDrawer"
                PropertyChanges { target: drawerIcon; anchors.leftMargin: -2*mm; }
            },
            State {
                name: "showDrawer"
                PropertyChanges { target: drawerIcon; anchors.leftMargin: -4*mm; }
            }
        ]

        transitions: Transition {
            from: "hideDrawer"; to: "showDrawer"; reversible: true
            NumberAnimation {
                properties: "anchors.leftMargin";
                easing {
                    type: Easing.OutInExpo;
                    overshoot: 400
                }
            }
        }
    }

    Image {
        id: logoIcon

        sourceSize.width: 5*mm
        sourceSize.height: 5*mm

        anchors{
            left: parent.left
            leftMargin: 5*mm
            verticalCenter: parent.verticalCenter
        }
        source: {
            var ppi = mm*25.4

             if (ppi>=540)
              "qrc:/img/images/icons/istodo_256.png"
             else if (ppi>=420)
              "qrc:/img/images/icons/istodo_128.png"
             else if (ppi>=320)
              "qrc:/img/images/icons/istodo_96.png"
             else if (ppi>=220)
              "qrc:/img/images/icons/istodo_64.png"
             else if (ppi>=160)
              "qrc:/img/images/icons/istodo_48.png"
             else
              "qrc:/img/images/icons/istodo_32.png"
             }
    }
    Rectangle {

        height: ACGlobal.headerHeight
        anchors {
            left: logoIcon.right
            leftMargin: 1*mm
            topMargin: 0
            verticalCenter: parent.verticalCenter
        }

        Column {
            id: textElements

            Text {
                id: viewName
                color: "white"
                font.pixelSize: 3.5*mm
            }

            Text {
                id: viewInfo
                color: "white"
                font.pixelSize: 2.5*mm
            }

            spacing: 0
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if( drawerIcon.state == "hideDrawer") {
                ACGlobal.navDrawer.showDrawer()
            }
            else{
                ACGlobal.navDrawer.hideDrawer()
            }
        }
    }

    Connections {
        target: ACGlobal.navDrawer

        onDrawerShowed: {
            drawerIcon.state = "showDrawer"
        }

        onDrawerClosed: {
            drawerIcon.state = "hideDrawer"
        }
    }
}
