INCLUDEPATH += $$PWD/..

QT += core sql xml network

SOURCES += \
    $$PWD/qdropbox.cpp \
    $$PWD/qdropboxaccount.cpp \
    $$PWD/qdropboxfile.cpp \
    $$PWD/qdropboxfileinfo.cpp \
    $$PWD/qdropboxjson.cpp

HEADERS  += \
    $$PWD/qdropbox.h \
    $$PWD/qdropboxaccount.h \
    $$PWD/qdropboxfile.h \
    $$PWD/qdropboxfileinfo.h \
    $$PWD/qdropboxjson.h \
    $$PWD/qtdropbox.h \
    $$PWD/qtdropbox_global.h
